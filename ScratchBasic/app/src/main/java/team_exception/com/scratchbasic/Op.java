package team_exception.com.scratchbasic;

/**
 * An abstract base class representing a JDUMBBASIC instruction.  They are generated from the source by the 
 * parser in OpMaker.
 * 
 * If you want to implement another operation, you'll need to:
 * 
 * Subclass this.  You will usually invoke the base class constructor, and you will need to implement the execute method.
 * add parsing instructions to OpMaker.
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public abstract class  Op {
	protected Integer nextln;
	protected Integer lineno;

	public Op(int line) {
		lineno = line;
		nextln = null; //this can't be set at construction time because it is not known then
	}

	/**
	 * executes the instruction.
	 *
	 * NOTE: The execute method is responsible for updating variables *and* the instruction pointer by invoking the appropriate methods in Context.
	 *
	 * @param context the program runtime state
	 * @throws Exception
	 */
	public abstract void execute(Context context) throws Exception;
	
	/**
	 * sets the next line number.
	 * 
	 * NOTE: This isn't in the constructor because the next line is not known until parsing is complete
	 *
	 * @param next line number to set the next line number to
	 */
	public void setNextLn(int next) {
		nextln=next;
	}
	
	/**
	 * gets the next line number
	 *
	 * @return the next line number
	 */
	public Integer getNextLn() {
		return nextln;
	}
	
	/**
	 * gets the current line number.
	 *
	 * NOTE: There is no corresponding setLineNo method (this is set in the constructor and is deliberately immutable).
	 * @return the current line number
	 */
	public Integer getLineNo() {
		return lineno;
	}

}
