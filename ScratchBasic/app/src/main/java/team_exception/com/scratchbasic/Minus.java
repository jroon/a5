package team_exception.com.scratchbasic;

/**
 * implements a subtraction expression
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class Minus extends Expression {

	public Minus(String left, String right) {
		super(left, right);
	}

	@Override
	public int evaluate(Context context) throws ValueNotPresentException {
		int leftval = context.getiValue(lhs);
		int rightval = context.getiValue(rhs);

		return leftval - rightval;
	}
}
