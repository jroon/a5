package team_exception.com.scratchbasic;

/**
 * A parser for expressions that creates one of the right type depending on the infix operator
 * specified.
 * 
 * Adding another Expression subclasses will require you to edit this class.
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class ExpressionMaker {

	public ExpressionMaker() {
		return;
	}
	
	/**
	 * Make an expression of the appropriate type
	 * 
	 * @param lhs the left hand side of the expression
	 * @param op the operator symbol
	 * @param rhs the right hand side of the expression.
	 * @return an expression object of appropriate type
	 * @throws MyParseException
	 */
	public Expression makeExpression(String lhs, String op, String rhs) throws MyParseException {
		switch (op ) {
		case "+":
			return new Plus(lhs, rhs);
		case "-":
			return new Minus(lhs, rhs);
		case "==":
			return new Equals(lhs, rhs);
		case ">":
			return new GreaterThan(lhs, rhs);
		default:
			throw new MyParseException("unrecognized expression operator");
		}
	}
}
