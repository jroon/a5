package team_exception.com.scratchbasic;

import java.util.Iterator;
import java.util.TreeMap;

/**
 * stores the compiled "bytecode" of a DUMBBASIC program.
 *
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class OpCollection {
	private TreeMap<Integer, Op> ops;
	private Integer firstop = null;
	
	/**
	 * Creates an OpCollection (DUMBBASIC source) from an input stream
	 * 
	 * @param args input stream
	 * @param index number of elements in input stream
	 * @throws MyParseException
	 */
	public OpCollection(String[] args, int index) throws MyParseException {
		OpMaker opm = new OpMaker();
		ops = new TreeMap<Integer, Op>();
		while(args[index] != null) {
			//System.out.println(nextline); //for debugging

			String nextline = args[index];
			Op nextop = opm.make(nextline);
			ops.put(nextop.getLineNo(), nextop);
			index += 1;
		}
		addNextLns();
		Iterator <Integer>linelist=ops.keySet().iterator();
		if (linelist.hasNext()) {
			firstop = linelist.next();
		}
		return;
	}

	/**
	 * Gets the Op corresponding to a given line number
	 *
	 * NOTE: The context is passed solely for the purposes of returning additional information in the event of an exception
	 *
	 * @param lineno line number of operation to return
	 * @param context context of program
	 * @return the op at lineno
	 * @throws LineNumberDoesNotExistException
	 */
	public Op getOp(Integer lineno, Context context) throws LineNumberDoesNotExistException {
		Op thisOp = ops.get(lineno);
		if (thisOp == null) {
			throw new LineNumberDoesNotExistException(context.getPrevIp().toString());
		}
		return thisOp;
	}

	/**
	 * Checks if a line number has an operation
	 *
	 * @param lineno line number to check for operation
	 * @return true if line number has an op, false otherwise
     */
	public Boolean hasOp(Integer lineno) {
		Op testOp = ops.get(lineno);
		if (testOp == null) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	/**
	 * Gets the first (lowest) line number in a DUMBBASIC program
	 *
	 * @return the first line number in a program
	 */
	public Integer getFirstLineno() {
		return firstop;
	}

	/**
	 * Adds the "next line" values to each Op in the collection
	 *
	 * NOTE: Should only be invoked when the program has been completed parsing
	 */
	private void addNextLns() {
		Iterator<Op> oplist = ops.values().iterator();
		if (!(oplist.hasNext())) {
			return;
		}
		Iterator<Integer> linenext = ops.keySet().iterator();
		linenext.next();
		while(linenext.hasNext()) {
			Op op = oplist.next();
			int lineno = linenext.next();
			op.setNextLn(lineno);
		}
	}
}
