package team_exception.com.scratchbasic;

/**
 * Exception to invoke if a RETURN tries to RETURN to a non-existent GOSUB
 *
 * @author Jae Ren
 */
public class InvalidReturnException extends Exception {

    public InvalidReturnException() {
        super();
    }

    public InvalidReturnException(String message) {
        super(message);
    }

    public InvalidReturnException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public InvalidReturnException(Throwable throwable) {
        super(throwable);
    }
}
