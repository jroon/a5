package team_exception.com.scratchbasic;

/**
 * The top-level main class for the JDUMBBASIC interpreter
 * 
 * See the README.md file for more information.
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class DumbbasicMain {
	public static MainActivity activity;

	public DumbbasicMain(MainActivity act){
		activity = act;
	}

	/**
	 * Creates and runs a program built from a string array
	 *
	 * @param args
	 */
	public static void execute(String[] args) {
		int index = 0;
		try {
			OpCollection ops= new OpCollection(args, index);
			run(ops);
		} catch (Exception e) {
			if (e.getClass().equals(ValueNotPresentException.class)){
				activity.addToConsole(". . . ");
				activity.addToConsole("ERROR: Variable not found!\n   Ensure all variables have been initialized using the LET block.\n   Exception encountered at line " + e.getMessage().toString() + ".");
				activity.error = true;
			}
			else if (e.getClass().equals(LineNumberDoesNotExistException.class)){
				activity.addToConsole(". . . ");
				activity.addToConsole("ERROR: Invalid line number!\n   Ensure all GOTO/IFGOTO/GOSUB blocks have valid line numbers.\n   Exception encountered at line " + e.getMessage().toString() + ".");
				activity.error = true;
			}
			else if (e.getClass().equals(MyParseException.class)){
				activity.addToConsole(". . . ");
				activity.addToConsole("ERROR: 42");
				activity.error = true;
			}
            else if (e.getClass().equals(InvalidReturnException.class)){
                activity.addToConsole(". . . ");
                activity.addToConsole("ERROR: Invalid RETURN statement!\n   Ensure all RETURN blocks have a corresponding GOSUB block.\n   Exception encountered at line " + e.getMessage().toString() + ".");
                activity.error = true;
            }
		}

	}

	/**
	 * The run loop
	 *
	 * @param ops
	 * @throws Exception
	 */
	private static void run(OpCollection ops) throws Exception {
		Context context = new Context();
        int firstline = ops.getFirstLineno();
		context.setIp(firstline);

		while(context.getIp() != null && !(activity.interrupt)) {
			Op thisop = ops.getOp(context.getIp(), context);
			thisop.execute(context);
		}
	}
}





