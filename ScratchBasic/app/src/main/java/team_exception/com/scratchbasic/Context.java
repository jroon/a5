package team_exception.com.scratchbasic;
import java.util.EmptyStackException;
import java.util.Stack;
import java.util.TreeMap;

/**
 * Stores the state of the running DUMBBASIC program, including variable values and instruction pointer
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 * 
 */
public class Context {
	private Integer ip;
	private Stack<Integer> gosubStack;
	public Integer prevIp;
	private TreeMap<String, Integer> symbol_table;
	private TreeMap<String, String> ssymbol_table; //for string variables

	public Context() {
		ip = null;
		symbol_table = new TreeMap<String, Integer>();
		ssymbol_table = new TreeMap<String, String>();
		gosubStack = new Stack<Integer>();
	}

	/**
	 * Gets the string value of a expression component
	 *
	 * @param varname literal integer to parse or variable name
	 * @return an integer representing the value of either the literal or the variable
	 * @throws ValueNotPresentException
	 */
	public String getsValue(String varname) throws ValueNotPresentException {
		String val;
		try {
			//if literal integer
			Integer.parseInt(varname);
			val = varname;
		} catch(NumberFormatException e) { //else variable
			if (!symbol_table.containsKey(varname)) {
				if (!ssymbol_table.containsKey(varname)) {
					throw new ValueNotPresentException(((Integer)ip).toString());
				}
				else {
					val = ssymbol_table.get(varname);
				}
			}
			else {
				val = symbol_table.get(varname).toString();
			}
		}
		return val;
	}

	/**
	 * Gets the integer value of a expression component
	 *
	 * @param varname literal integer to parse or variable name
	 * @return an integer representing the value of either the literal or the variable
	 * @throws ValueNotPresentException
	 */
	public int getiValue(String varname) throws ValueNotPresentException {
		int val;
		try {
			val = Integer.parseInt(varname);
		} catch (NumberFormatException e) {
			if (!symbol_table.containsKey(varname)){
				throw new ValueNotPresentException(((Integer)ip).toString());
			}
			else {
				val = symbol_table.get(varname);
			}
		}
		return val;
	}

	/**
	 * Sets the value of a variable to a value
	 *
	 * @param variable - the name of the variable
	 * @param value - the integer value to set it to
	 */
	public void setValue(String variable, int value) {
		symbol_table.put(variable, value);
		return;
	}

	/**
	 * Sets the value of a variable to a string
	 *
	 * @param variable - the name of the variable
	 * @param value - the string value to set it to
	 */
	public void setValue(String variable, String value){
		ssymbol_table.put(variable, value);
		return;
	}

	/**
	 * Sets the instruction pointer
	 * 
	 * @param lineno new line number to set the instruction pointer to
	 */
	public void setIp(Integer lineno) {
		// TODO Auto-generated method stub
		prevIp = ip;
		ip = lineno;
		return;
	}

	/**
	 * Gets the instruction pointer
	 *
	 * @return the instruction pointer
	 */
	public Integer getIp() {
		return ip;
	}

	/**
	 * Gets the previous instruction pointer
	 *
	 * @return the preceding instruction pointer
	 */
	public Integer getPrevIp(){
		return prevIp;
	}

	/**
	 * Pushes a new line number onto the gosub stack
	 *
	 * @param lineno line number to be pushed onto gosub stack
     */
	public void pushGosubIp(Integer lineno){
		gosubStack.push(lineno);
	}

	/**
	 * Pops a line number off the gosub stack
	 *
	 * @return the last line number pushed onto the gosub stack
	 * @throws InvalidReturnException
     */
	public Integer popGosubIp() throws InvalidReturnException{
		try {
			return (gosubStack.pop());
		} catch (EmptyStackException e){
			throw new InvalidReturnException(ip.toString());
		}
	}
}
