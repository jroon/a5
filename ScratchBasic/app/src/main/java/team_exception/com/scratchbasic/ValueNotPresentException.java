package team_exception.com.scratchbasic;

/**
 * Exception to invoke if a DUMBBASIC variable is used before its value is defined by a LET statement
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class ValueNotPresentException extends Exception {

	private static final long serialVersionUID = -3351147583689152318L;

	public ValueNotPresentException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ValueNotPresentException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ValueNotPresentException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ValueNotPresentException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
