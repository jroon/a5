package team_exception.com.scratchbasic;

/**
 * Implements the GOSUB operation
 *
 * @author Jae Ren
 *
 */
public class Gosub extends Op{

    private String target;

    /**
     * creates a GOSUB op
     *
     * @param line - the current line number
     * @param tgt - either a string literal of a number, or a variable name, which represents where we should GO TO
     */
    public Gosub(int line, String tgt){
        super(line);
        target = tgt;
        return;
    }

    @Override
    public void execute(Context context) throws Exception {
        int newline = context.getiValue(target);
        context.pushGosubIp(nextln);
        context.setIp(newline);
        return;
    }
}
