package team_exception.com.scratchbasic;

/**
 * Abstract base class for all expressions in DUMBBASIC.
 * 
 * In DUMBBASIC, the base class supports two operands lhs and rhs, which the subclasses have
 * direct access to (this might be a wart, but anyway).
 * 
 * Like Op, adding new expressions requires subclassing Expression, and editing ExpressionMaker to
 * support parsing the new Expression.  The constructor should
 * probably invoke the base class constructor, and an implementation for evaluate is required.
 * 
 *  @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public abstract class Expression{
	protected String lhs;
	protected String rhs;

	/**
	 * Evaluate the expression and return the value of that expression given the current context
	 * 
	 * @param context - the runtime context
	 * @return the calculated value.
	 * @throws ValueNotPresentException
	 */
	public abstract int evaluate(Context context) throws ValueNotPresentException;
	
	/**
	 * constructor which handles two operands
	 *
	 * @param left - the "left" operand in a conventional infix expression
	 * @param right - the "right" operand in a conventional infix expression.
	 */
	Expression(String left, String right) {
		rhs = right;
		lhs = left;
		return;
	}
}
