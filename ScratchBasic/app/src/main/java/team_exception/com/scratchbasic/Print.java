package team_exception.com.scratchbasic;

import android.app.Fragment;
/**
 * Prints a constant, string, or the value of a variable to console
 *
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class Print extends Op {

	private String value;

	public Print(int line, String val) {
		super(line);
		value = val;
	}

	@Override
	public void execute(Context context) throws Exception {
		String toPrint;
		if (!value.contains("$")) { //if value to print is not string
			toPrint = context.getsValue(value);
		}
		else {
			toPrint = value.replace("$", "");
		}
		DumbbasicMain.activity.addToConsole(toPrint);
		Thread.sleep(10); //delay to allow ui thread to pickup stop event
		context.setIp(getNextLn());
	}

}
