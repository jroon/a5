package team_exception.com.scratchbasic;

/**
 * Exception to invoke if a GOTO or IF-GOTO tries to GOTO a non-existent line number
 *
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class LineNumberDoesNotExistException extends Exception {

	public LineNumberDoesNotExistException() {
		super();
	}

	public LineNumberDoesNotExistException(String message) {
		super(message);
	}

	public LineNumberDoesNotExistException(Throwable arg0) {
		super(arg0);
	}

	public LineNumberDoesNotExistException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
