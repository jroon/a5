package team_exception.com.scratchbasic;

/**
 * Implements the LET operation
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class Let extends Op {
	private String var; // name of the variable whose value is set in this operation
	private Expression exp; // the expression to evaluate
	private String val;
	
	/**
	 * creates a LET op of type expression
	 *
	 * @param line line number of this op
	 * @param varname varname to set
	 * @param expression expression to evaluate when executed
	 */
	public Let(int line, String varname, Expression expression) {
		super(line);
		var=varname;
		exp=expression;
		return;
	}

	/**
	 * creates a LET op of type string
	 *
	 * @param line line number of this op
	 * @param varname varname to set
	 * @param value string
	 */
	public Let(int line, String varname, String value) {
		super(line);
		var=varname;
		val=value;
		return;
	}

	@Override
	public void execute(Context context) throws Exception {
		if (exp != null){
			int expval = exp.evaluate(context);
			context.setValue(var, expval);
		}
		else {
			context.setValue(var, val);
		}
		context.setIp(nextln);
		return;

	}

}
