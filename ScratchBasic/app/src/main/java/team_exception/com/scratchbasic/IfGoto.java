package team_exception.com.scratchbasic;

/**
 * implements the IF (expression) GOTO (target) statement
 *
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class IfGoto extends Op {
	private Expression expression;
	private String target;

	/**
	 * creates an IF-GOTO op
	 * 
	 * @param line - the current line number
	 * @param exp - the expression that must evaluate to true (non-zero) for the GOTO to occur
	 * @param tgt - where to go if expression is true.  can be a numeric literal or a variable name
	 */
	public IfGoto(int line, Expression exp, String tgt) {
		super(line);
		expression=exp;
		target = tgt;
		return;
	}

	@Override
	public void execute(Context context) throws Exception {
		int expval = expression.evaluate(context);
		if (expval != 0) {
			int targetval = context.getiValue(target);
			context.setIp(targetval);

		} else {
			context.setIp(nextln);
		}
		return;
	}

}
