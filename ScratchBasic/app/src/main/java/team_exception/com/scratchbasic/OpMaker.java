package team_exception.com.scratchbasic;

/**
 * Turns a line of DUMBBASIC into an op
 * 
 * NOTE: Adding a new Op to the language will require adding code to this class.
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class OpMaker {

	private ExpressionMaker exp_maker;
	public OpMaker() {
		 exp_maker=new ExpressionMaker();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Turns a line of text into an Op.
	 *
	 * NOTE: Modify this method if a new Op is added
	 *
	 * @param ops string (line) of DUMBBASIC to parse
	 * @return an Op object of appropriate type
	 * @throws MyParseException
	 */
	public Op make(String ops) throws MyParseException {
		String []tokens = ops.split("\\s+"); // Split on whitespace
		/*for (String a : tokens){
			System.out.println(a.toString());
		}*/ //FOR DEBUG

		if (tokens.length < 2) {
			throw new MyParseException();
		}

		int lineno;
		try {
			lineno = Integer.parseInt(tokens[0]);
		} catch (NumberFormatException e) {
			throw new MyParseException();
		}

		switch (tokens[1]) {
			case "REM": // LINENO_0 REM_1 COMMENT_2
				return new Remark(lineno);

			case "PRINT": // LINENO_0 PRINT_1 VALUE_2
				try {
					return new Print(lineno, tokens[2]);
				} catch (Exception e) {
					throw new MyParseException();
				}

			case "GOTO": // LINENO_0 GOTO_1 TARGET_2
				try {
					return new Goto(lineno, tokens[2]);

				} catch (Exception e) {
					throw new MyParseException();
				}
			case "LET": // LINENO_0 LET_1 VARIABLE_2 =_3 LHS_4 OP_5 RHS_6
				String variable = tokens[2];
				if (tokens[tokens.length-1].contains("$")){ //if case string variable

					//get string value
					String value = "";
					value += tokens[4];
					for (int i=5; i<tokens.length; ++i){
						value += " ";
						value += tokens[i];
					}
					value = value.replace("$", "");

					return new Let(lineno, variable, value);
				}
				else { //else case integer variable
					Expression letexp = exp_maker.makeExpression(tokens[4], tokens[5], tokens[6]);
					return new Let(lineno, variable, letexp);
				}

			case "IF": // LINENO_0 IF_1 LHS_2 OP_3 RHS_4 GOTO_5 TARGET_6
				try {
					Expression ifgotoexp = exp_maker.makeExpression(tokens[2], tokens[3], tokens[4]);
					return new IfGoto(lineno, ifgotoexp, tokens[6]);
				} catch (Exception e) {
					throw new MyParseException();
				}

			case "GOSUB": // LINENO_0 GOSUB_1 TARGET_2
				try {
					return new Gosub(lineno, tokens[2]);
				} catch (Exception e) {
					throw new MyParseException();
				}

			case "RETURN": // LINENO_0 RETURN_1
				try {
					return new Return(lineno);
				} catch (Exception e) {
					throw new MyParseException();
				}

			default:
				throw new MyParseException();
		}
	}
}
  