package team_exception.com.scratchbasic;

/**
 * Implements an addition expression
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class Plus extends Expression {

	public Plus(String left, String right) {
		super(left, right);
	}

	@Override
	public int evaluate(Context context) throws ValueNotPresentException {
		int leftval = context.getiValue(lhs);
		int rightval = context.getiValue(rhs);

		return leftval + rightval;
	}
}
