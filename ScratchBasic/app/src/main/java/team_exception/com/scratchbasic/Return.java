package team_exception.com.scratchbasic;

/**
 * Implements the RETURN operation
 *
 * @author Jae Ren
 */
public class Return extends Op{

    /**
     * creates a RETURN op
     *
     * @param line - the current line number
     */
    public Return(int line) {
        super(line);
    }

    @Override
    public void execute(Context context) throws Exception {
        context.setIp(context.popGosubIp());
        return;
    }
}
