package team_exception.com.scratchbasic;

/**
 * Exception to invoke when the structure of a DUMBBASIC statement does not match the permitted forms.
 *
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class MyParseException extends Exception {

	private static final long serialVersionUID = 3334936271124777769L;

	public MyParseException() {
		super();
	}

	public MyParseException(String message) {
		super(message);
	}

	public MyParseException(Throwable cause) {
		super(cause);
	}

	public MyParseException(String message, Throwable cause) {
		super(message, cause);
	}

}
