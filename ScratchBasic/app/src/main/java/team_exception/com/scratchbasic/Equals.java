package team_exception.com.scratchbasic;

/**
 * Implements the "equality" (==) logical operation.
 *
 * By convention, in DUMBBASIC logical operators return 1 if true, 0 otherwise.
 *
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class Equals extends Expression {

	public Equals(String left, String right) {
		super(left, right);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int evaluate(Context context) throws ValueNotPresentException {
		int leftval = context.getiValue(lhs);
		int rightval = context.getiValue(rhs);

	
		if (leftval == rightval) {
			return 1;
		}else {
			return 0;
		}
	}

}
