package team_exception.com.scratchbasic;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.Arrays;
import android.widget.ArrayAdapter;
import java.util.List;

/**
 * Main activity class of application
 *
 * View the readme file for more information
 *
 * @author Lee Yen
 * @author Jae Ren
 *
 */

public class MainActivity extends Activity {
    TextView remBlock;
    TextView letBlock;
    TextView printBlock;
    TextView gotoBlock;
    TextView ifGotoBlock;
    TextView gosubBlock;
    TextView returnBlock;

    TextView plusBlock;
    TextView minusBlock;
    TextView greaterThanBlock;
    TextView equalityBlock;

    TextView variableBlock;

    LinearLayout programGrid;

    TextView console;

    String[] lineContent; //string array representation of program

    List<String> variables; //contains names of default variables
    String[] instructions; //contains names of instruction blocks for comparison purposes
    String[] expressions; //contains names of expression blocks for comparison purposes

    //program execution flags
    boolean interrupt;
    boolean error;

    int maxLineNo = 19; //maximum program line number

    /**
     * Initializes all blocks with drag listeners
     * Initializes program grid and console output
     * Initializes and fills variable list and instruction list
     */
    private void init() {
        remBlock = (TextView) findViewById(R.id.rem_block);
        letBlock = (TextView) findViewById(R.id.let_block);
        printBlock = (TextView) findViewById(R.id.print_block);
        gotoBlock = (TextView) findViewById(R.id.goto_block);
        ifGotoBlock = (TextView) findViewById(R.id.if_block);
        gosubBlock = (TextView) findViewById(R.id.gosub_block);
        returnBlock = (TextView) findViewById(R.id.return_block);

        plusBlock = (TextView) findViewById(R.id.exp_plus_block);
        minusBlock = (TextView) findViewById(R.id.exp_minus_block);
        greaterThanBlock = (TextView) findViewById(R.id.exp_greater_block);
        equalityBlock = (TextView) findViewById(R.id.exp_equals_block);
        variableBlock = (TextView) findViewById(R.id.var_block);

        remBlock.setOnTouchListener(new TouchDrag());
        letBlock.setOnTouchListener(new TouchDrag());
        printBlock.setOnTouchListener(new TouchDrag());
        gotoBlock.setOnTouchListener(new TouchDrag());
        ifGotoBlock.setOnTouchListener(new TouchDrag());
        gosubBlock.setOnTouchListener(new TouchDrag());
        returnBlock.setOnTouchListener(new TouchDrag());
        plusBlock.setOnTouchListener(new TouchDrag());
        minusBlock.setOnTouchListener(new TouchDrag());
        greaterThanBlock.setOnTouchListener(new TouchDrag());
        equalityBlock.setOnTouchListener(new TouchDrag());

        variableBlock.setOnTouchListener(new TouchDrag());

        programGrid = (LinearLayout) findViewById(R.id.program_grid); // Init the command grid, for user generated code

        console = (TextView) findViewById(R.id.console_grid); // Init the console, for displaying output
        console.setMovementMethod(new ScrollingMovementMethod());

        variables = new ArrayList<String>(); // Init variable dropdown list

        instructions = new String[] {"rem", "let", "print", "goto", "ifGoto", "gosub", "return"};
        expressions = new String[] {"plus", "minus", "greaterThan", "equality"};

    }

    /**
     * Initializes the string array representation of the program
     * Initializes and fills the program grid with lines
     */
    private void initLines() {
        lineContent = new String[maxLineNo + 1];

        for (int lineNum = 0; lineNum <= maxLineNo; lineNum++) {
            //init line and set listener
            LinearLayout line = (LinearLayout) getLayoutInflater().inflate(R.layout.line, null);
            line.setTag(lineNum);

            //add line number to layout
            TextView numberBlock = (TextView) getLayoutInflater().inflate(R.layout.number, null);
            numberBlock.setText(Integer.toString(lineNum));
            numberBlock.setOnLongClickListener(new LongClickDrag());
            numberBlock.setTag(((Integer)lineNum).toString());

            line.addView(numberBlock);

            //catch drop listener
            line.setOnDragListener(new InstructionListener());

            programGrid.addView(line);
        }
    }

    /**
     * Resets the program grid
     * Resets the string array representation of program
     */
    private void clearLines() {
        for (int lineNum = 0; lineNum <= maxLineNo; lineNum++) {
            if (lineContent[lineNum] != null) {
                LinearLayout line = (LinearLayout) programGrid.getChildAt(lineNum);
                line.removeViewAt(1);
            }
        }

        lineContent = new String[maxLineNo + 1];
    }

    /**
     * Initializes the activity
     *
     * @param savedInstanceState default argument from override
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        init();
        initLines();
    }

    /**
     * Confirms user exit
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit the application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    /**
     * Prints a string onto the console view
     *
     * @param text string to be printed onto the console
     */
    public void addToConsole(String text) {

        runOnUiThread(new Runnable() { //run on ui to enable instant ui update/feedback
            private String text;

            public Runnable init(String txt) {
                text = txt;
                return(this);
            }

            @Override
            public void run() {
                console.append("\n> " + text); //append text

                int scroll = console.getLayout().getLineTop(console.getLineCount()) - console.getHeight(); //auto scroll to last line
                if (scroll > 0){
                    console.scrollTo(0, scroll);
                } else {
                    console.scrollTo(0, 0);
                }
            }
        }.init(text));
    }

    /**
     * Touch listener for drag functionality
     */
    public class TouchDrag implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent m) {
            ClipData clip = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
            v.startDrag(clip, shadowBuilder, v, 0);
            return true;
        }
    }

    /**
     * Long click listener for drag functionality
     */
    public class LongClickDrag implements View.OnLongClickListener {
        @Override
        public boolean onLongClick(View v) {
            ClipData clip = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
            v.startDrag(clip, shadowBuilder, v, 0);
            return true;
        }
    }

    /**
     * Drop listener for instructions
     *
     * All instructions are tagged as follows:
     * # is the standard tag for all values and variables
     * - is the standard tag for unset values, it is removed forever upon the first setting of the value
     *
     * NOTE: Only the REM block has no "-" tag, as the program can still run without it being set
     */
    public class InstructionListener implements View.OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();

            TextView draggedBlock = (TextView) event.getLocalState();
            LinearLayout line = (LinearLayout) v;
            int lineNum = (int) line.getTag(); //get line number

            String lineString = lineContent[lineNum]; //get corresponding string array representation of program

            if (action == DragEvent.ACTION_DROP) {
                String blockType = (String) draggedBlock.getTag(); //get type of instruction

                if (lineString == null && Arrays.asList(instructions).contains(blockType)) { //if line is empty and instruction block is dropped

                    LinearLayout instruc;

                    switch (blockType) {
                        case "rem":
                            instruc = (LinearLayout) getLayoutInflater().inflate(R.layout.block_rem, null);
                            instruc.getChildAt(0).setOnLongClickListener(new DeleteListener());

                            instruc.setTag("rem");
                            line.addView(instruc);

                            instruc.getChildAt(1).setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    final TextView remContentBlock = (TextView) v;
                                    LinearLayout line = (LinearLayout) remContentBlock.getParent().getParent();
                                    final int lineNum = (int) line.getTag();

                                    final EditText input = new EditText(MainActivity.this);

                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                                            .setTitle("Set Remark")

                                            .setView(input)

                                            .setCancelable(false)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    String remark = input.getText().toString();
                                                    remContentBlock.setText(remark);

                                                    lineContent[lineNum] = lineContent[lineNum].replace(remContentBlock.getTag().toString(), "#" + remark);
                                                    remContentBlock.setTag("#" + remark); //change tag identifier for when value is changed repeatedly

                                                    dialog.dismiss();
                                                }
                                            });

                                    builder.create().show();
                                }
                            });

                            instruc.getChildAt(1).setTag("#0"); //set tag of content block

                            lineContent[lineNum] = "REM #0"; //modify string array representation of program as required

                            break;

                        case "let":
                            instruc = (LinearLayout) getLayoutInflater().inflate(R.layout.block_let, null);
                            instruc.getChildAt(0).setOnLongClickListener(new DeleteListener());
                            instruc.getChildAt(2).setOnLongClickListener(new DeleteListener());

                            instruc.setTag("let");
                            line.addView(instruc); // Append view to the line it was dragged onto

                            instruc.getChildAt(1).setOnDragListener(new VariableListener());
                            instruc.getChildAt(1).setTag("#-0");
                            instruc.getChildAt(3).setOnDragListener(new ExpressionListener());
                            instruc.getChildAt(3).setTag("#-exp");

                            lineContent[lineNum] = "LET #-0 = #-exp";

                            break;

                        case "print":
                            instruc = (LinearLayout) getLayoutInflater().inflate(R.layout.block_print, null);
                            instruc.getChildAt(0).setOnLongClickListener(new DeleteListener());

                            instruc.setTag("print");
                            line.addView(instruc); // Append the view to the line it was dragged onto

                            instruc.getChildAt(1).setOnClickListener(new ValueListener());
                            instruc.getChildAt(1).setOnDragListener(new VariableListener());
                            instruc.getChildAt(1).setTag("#-0"); //used for modifying text

                            lineContent[lineNum] = "PRINT #-0";

                            break;

                        case "goto":
                            instruc = (LinearLayout) getLayoutInflater().inflate(R.layout.block_goto, null);
                            instruc.getChildAt(0).setOnLongClickListener(new DeleteListener());

                            instruc.setTag("goto");
                            line.addView(instruc);

                            instruc.getChildAt(1).setOnDragListener(new LineNumberListener());
                            instruc.getChildAt(1).setTag("#-0");

                            lineContent[lineNum] = "GOTO #-0";

                            break;

                        case "ifGoto":
                            instruc = (LinearLayout) getLayoutInflater().inflate(R.layout.block_if, null);
                            instruc.getChildAt(0).setOnLongClickListener(new DeleteListener());
                            instruc.getChildAt(2).setOnLongClickListener(new DeleteListener());

                            instruc.setTag("ifGoto");
                            line.addView(instruc);

                            instruc.getChildAt(1).setOnDragListener(new ExpressionListener());
                            instruc.getChildAt(1).setTag("#-exp");
                            instruc.getChildAt(3).setOnDragListener(new LineNumberListener());
                            instruc.getChildAt(3).setTag("#-0");

                            lineContent[lineNum] = "IF #-exp GOTO #-0";

                            break;

                        case "gosub":
                            instruc = (LinearLayout) getLayoutInflater().inflate(R.layout.block_gosub, null);
                            instruc.getChildAt(0).setOnLongClickListener(new DeleteListener());

                            instruc.setTag("gosub");
                            line.addView(instruc);

                            instruc.getChildAt(1).setOnDragListener(new LineNumberListener());
                            instruc.getChildAt(1).setTag("#-0");

                            lineContent[lineNum] = "GOSUB #-0";

                            break;

                        case "return":
                            instruc = (LinearLayout) getLayoutInflater().inflate(R.layout.block_return, null);
                            instruc.getChildAt(0).setOnLongClickListener(new DeleteListener());

                            instruc.setTag("return");
                            line.addView(instruc);

                            lineContent[lineNum] = "RETURN";

                            break;

                        default:
                            break;
                    }
                }
            }
            return true;
        }
    }

    /**
     * Drop listener for expressions
     *
     * All expressions are tagged as follows:
     * # is the standard tag for all values and variables
     * - is the standard tag for unset values, it is removed forever upon the first setting of the value
     * [ denotes the left hand side of the expression
     * ] denotes the right hand side of the expression
     */
    public class ExpressionListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();

            TextView expressionContentBlock = (TextView) v;
            TextView draggedBlock = (TextView) event.getLocalState();
            String blockType = (String) draggedBlock.getTag(); //get type of expression

            if (action == DragEvent.ACTION_DROP) {
                LinearLayout expressionContentBlockParent = (LinearLayout) expressionContentBlock.getParent();
                int expressionContentBlockIndex = expressionContentBlockParent.indexOfChild(expressionContentBlock);

                LinearLayout line = (LinearLayout) expressionContentBlock.getParent().getParent();
                int lineNum = (int) line.getTag(); //get line number

                LinearLayout newExpression;

                switch (blockType) {
                    case "plus":
                        expressionContentBlockParent.removeViewAt(expressionContentBlockIndex); //remove original block (to be replaced with expression block)

                        newExpression = (LinearLayout) getLayoutInflater().inflate(R.layout.block_plus, null);

                        //set listeners and tags of content blocks
                        newExpression.getChildAt(0).setOnClickListener(new ValueListener());
                        newExpression.getChildAt(0).setOnDragListener(new VariableListener());
                        newExpression.getChildAt(0).setTag("#-[");
                        newExpression.getChildAt(2).setOnClickListener(new ValueListener());
                        newExpression.getChildAt(2).setOnDragListener(new VariableListener());
                        newExpression.getChildAt(2).setTag("#-]");

                        expressionContentBlockParent.addView(newExpression, expressionContentBlockIndex);

                        lineContent[lineNum] = lineContent[lineNum].replace("#-exp", "#-[ + #-]"); //modify string array representation of program as required

                        break;

                    case "minus":
                        expressionContentBlockParent.removeViewAt(expressionContentBlockIndex);

                        newExpression = (LinearLayout) getLayoutInflater().inflate(R.layout.block_minus, null);

                        newExpression.getChildAt(0).setOnClickListener(new ValueListener());
                        newExpression.getChildAt(0).setOnDragListener(new VariableListener());
                        newExpression.getChildAt(0).setTag("#-[");
                        newExpression.getChildAt(2).setOnClickListener(new ValueListener());
                        newExpression.getChildAt(2).setOnDragListener(new VariableListener());
                        newExpression.getChildAt(2).setTag("#-]");

                        expressionContentBlockParent.addView(newExpression, expressionContentBlockIndex);

                        lineContent[lineNum] = lineContent[lineNum].replace("#-exp", "#-[ - #-]");

                        break;

                    case "greaterThan":
                        expressionContentBlockParent.removeViewAt(expressionContentBlockIndex);

                        newExpression = (LinearLayout) getLayoutInflater().inflate(R.layout.block_greaterthan, null);

                        newExpression.getChildAt(0).setOnClickListener(new ValueListener());
                        newExpression.getChildAt(0).setOnDragListener(new VariableListener());
                        newExpression.getChildAt(0).setTag("#-[");
                        newExpression.getChildAt(2).setOnClickListener(new ValueListener());
                        newExpression.getChildAt(2).setOnDragListener(new VariableListener());
                        newExpression.getChildAt(2).setTag("#-]");

                        expressionContentBlockParent.addView(newExpression, expressionContentBlockIndex);

                        lineContent[lineNum] = lineContent[lineNum].replace("#-exp", "#-[ > #-]");

                        break;

                    case "equality":

                        expressionContentBlockParent.removeViewAt(expressionContentBlockIndex);

                        newExpression = (LinearLayout) getLayoutInflater().inflate(R.layout.block_equality, null);
                        newExpression.getChildAt(0).setOnClickListener(new ValueListener());
                        newExpression.getChildAt(0).setOnDragListener(new VariableListener());
                        newExpression.getChildAt(0).setTag("#-[");
                        newExpression.getChildAt(2).setOnClickListener(new ValueListener());
                        newExpression.getChildAt(2).setOnDragListener(new VariableListener());
                        newExpression.getChildAt(2).setTag("#-]");

                        expressionContentBlockParent.addView(newExpression, expressionContentBlockIndex);

                        lineContent[lineNum] = lineContent[lineNum].replace("#-exp", "#-[ == #-]");

                        break;
                }
            } else if (action == DragEvent.ACTION_DRAG_ENTERED) {
                if (Arrays.asList(expressions).contains(blockType)) {
                    expressionContentBlock.setBackgroundResource(R.drawable.style_variable_hover);
                }
            } else if (action == DragEvent.ACTION_DRAG_EXITED) {
                expressionContentBlock.setBackgroundResource(R.drawable.style_equation);
            }
            return true;
        }
    }

    /**
     * Drop listener for variables
     *
     * All variables are tagged as follows:
     * # is the standard tag for all values and variables
     * - is the standard tag for unset values, it is removed forever upon the first setting of the value
     * [ denotes the left hand side of the expression
     * ] denotes the right hand side of the expression
     */
    public class VariableListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();

            final TextView variableContentBlock = (TextView) v;
            TextView draggedBlock = (TextView) event.getLocalState();
            String blockType = (String) draggedBlock.getTag();

            LinearLayout line = (LinearLayout) variableContentBlock.getParent().getParent();
            if (Arrays.asList(instructions).contains(line.getTag())) { //if called within expression, get parent once more
                line = (LinearLayout) line.getParent();
            }
            final int lineNum = (int) line.getTag(); //get line number

            if (action == DragEvent.ACTION_DROP) {
                if (blockType.equals("var")) {
                    if (line.getChildAt(1).getTag().toString().equals("let") && !variableContentBlock.getTag().toString().matches(".*\\[.*|.*\\].*")) { //if first content block of LET block

                        final EditText input = new EditText(MainActivity.this);

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Set New Variable")
                                .setView(input)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (input.getText().toString().equals("")){ //if user clicked "OK" without any input
                                            dialog.dismiss();
                                        }
                                        else {
                                            String newValue = input.getText().toString();
                                            String tagIdentifier = "";

                                            if (variableContentBlock.getTag().toString().contains("[")) {
                                                tagIdentifier = "[";
                                            } else if (variableContentBlock.getTag().toString().contains("]")) {
                                                tagIdentifier = "]";
                                            }

                                            variableContentBlock.setText(newValue);
                                            variables.add(newValue);

                                            lineContent[lineNum] = lineContent[lineNum].replace(variableContentBlock.getTag().toString(), "#" + tagIdentifier + newValue); //modify string array representation of program as required
                                            variableContentBlock.setTag("#" + tagIdentifier + newValue); //change tag identifier for when value is changed repeatedly

                                            dialog.dismiss();
                                        }
                                    }
                                });

                        builder.create().show();

                        variableContentBlock.setBackgroundResource(R.drawable.style_variable);
                    }

                    else { //else normal variable content block

                        if (variables.size() == 0) { //if no variable have been initialized by a LET block

                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("Cannot Set Variable!")
                                    .setMessage("Initialize at least one variable using a LET block first.")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                            builder.create().show();

                            variableContentBlock.setBackgroundResource(R.drawable.style_value);

                        }
                        else { //else there are existing variables

                            //create spinner
                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, variables);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            final Spinner spinner = new Spinner(MainActivity.this);
                            spinner.setAdapter(dataAdapter);
                            spinner.setPadding(10, 0, 30, 0);

                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("Set Variable")
                                    .setView(spinner)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            String newValue = spinner.getSelectedItem().toString();
                                            String tagIdentifier = "";

                                            if (variableContentBlock.getTag().toString().contains("[")) {
                                                tagIdentifier = "[";
                                            } else if (variableContentBlock.getTag().toString().contains("]")) {
                                                tagIdentifier = "]";
                                            }

                                            variableContentBlock.setText(newValue);

                                            lineContent[lineNum] = lineContent[lineNum].replace(variableContentBlock.getTag().toString(), "#" + tagIdentifier + newValue); //modify string array representation of program as required
                                            variableContentBlock.setTag("#" + tagIdentifier + newValue); //change tag identifier for when value is changed repeatedly

                                            variableContentBlock.setBackgroundResource(R.drawable.style_variable);

                                            dialog.dismiss();
                                        }
                                    });

                            builder.create().show();

                            if (variables.contains(variableContentBlock.getText().toString())) {
                                variableContentBlock.setBackgroundResource(R.drawable.style_variable);
                            }
                            else {
                                variableContentBlock.setBackgroundResource(R.drawable.style_value);
                            }
                        }
                    }
                }
            }
            if (action == DragEvent.ACTION_DRAG_ENTERED) {
                if (blockType.equals("var")) {
                    variableContentBlock.setBackgroundResource(R.drawable.style_variable_hover);
                }
            }
            if (action == DragEvent.ACTION_DRAG_EXITED) {
                if (line.getChildAt(1).getTag().toString().equals("let") && !variableContentBlock.getTag().toString().matches(".*\\[.?|.*\\].?")) { //if first content block of LET block
                    variableContentBlock.setBackgroundResource(R.drawable.style_variable);
                }
                else {
                    variableContentBlock.setBackgroundResource(R.drawable.style_value);
                }
            }
            return true;
        }
    }

    /**
     * Click listener for values
     *
     * All values are tagged as follows:
     * # is the standard tag for all values and variables
     * - is the standard tag for unset values, it is removed forever upon the first setting of the value
     * [ denotes the left hand side of the expression, if available
     * ] denotes the right hand side of the expression, if available
     */
    public class ValueListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final TextView valueContentBlock = (TextView) v;

            LinearLayout line = (LinearLayout) valueContentBlock.getParent().getParent();
            if (Arrays.asList(instructions).contains(line.getTag())) { //if called within expression, get parent once more
                line = (LinearLayout) line.getParent();
            }
            final int lineNum = (int) line.getTag(); //get line number

            final EditText input = new EditText(MainActivity.this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER); //set input to accept numbers only

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Set Value")
                    .setView(input)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String newValue = input.getText().toString();

                            if (newValue.equals("")) { //if user clicked "OK" without any input
                                dialog.dismiss();

                                if (variables.contains(valueContentBlock.getText().toString())) {
                                    valueContentBlock.setBackgroundResource(R.drawable.style_variable);
                                }
                                else {
                                    valueContentBlock.setBackgroundResource(R.drawable.style_value);
                                }
                            }
                            else{

                                valueContentBlock.setText(newValue);
                                String tagIdentifier = "";

                                if (((String) valueContentBlock.getTag()).contains("[")) { //if left hand side of expression
                                    tagIdentifier = "[";
                                }
                                else if (((String) valueContentBlock.getTag()).contains("]")){ //else if right hand side of expression
                                    tagIdentifier = "]";
                                }

                                lineContent[lineNum] = lineContent[lineNum].replace(valueContentBlock.getTag().toString(), "#" + tagIdentifier + newValue); //modify string array representation of program as required
                                valueContentBlock.setTag("#" + tagIdentifier + newValue); //change tag identifier for when value is changed repeatedly

                                dialog.dismiss();
                            }
                        }

                    });

            builder.create().show();

            valueContentBlock.setBackgroundResource(R.drawable.style_value);
        }
    }

    /**
     * Drop listener for line numbers
     *
     * All line numbers are tagged similarly to values and variables
     */
    public class LineNumberListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();

            final TextView lineNumContentBlock = (TextView) v;
            TextView draggedBlock = (TextView) event.getLocalState();
            String blockType = (String) draggedBlock.getTag(); //get type of line number (variable vs. explicit)

            LinearLayout line = (LinearLayout) lineNumContentBlock.getParent().getParent();
            final int lineNum = (int) line.getTag(); //get line number

            if (action == DragEvent.ACTION_DROP) {
                if (blockType.equals("var")) {

                    //create spinner
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, variables);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    final Spinner spinner = new Spinner(MainActivity.this);
                    spinner.setAdapter(dataAdapter);
                    spinner.setPadding(10, 0, 30, 0);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Set Variable")
                            .setView(spinner)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String newValue = spinner.getSelectedItem().toString();
                                    lineNumContentBlock.setText(newValue);

                                    lineContent[lineNum] = lineContent[lineNum].replace(lineNumContentBlock.getTag().toString(), "#" + newValue); //modify string array representation of program as required
                                    lineNumContentBlock.setTag("#" + newValue); //change tag identifier for when the value is changed repeatedly

                                    dialog.dismiss();
                                }
                            });

                    builder.create().show();

                    lineNumContentBlock.setBackgroundResource(R.drawable.style_variable);

                }
                else if (blockType.matches("\\d+(?:\\.\\d+)?")) { //if explicit line number block
                    String gotoLineNum = blockType; //change variable name to aid readability
                    lineNumContentBlock.setText(gotoLineNum);

                    lineContent[lineNum] = lineContent[lineNum].replace(lineNumContentBlock.getTag().toString(), "#" + gotoLineNum); //modify string array representation of program as required
                    lineNumContentBlock.setTag("#" + gotoLineNum); //change tag identifier for when the value is changed repeatedly

                    lineNumContentBlock.setBackgroundResource(R.drawable.style_number);
                }
            }
            if (action == DragEvent.ACTION_DRAG_ENTERED) {
                if (blockType.matches("\\d+(?:\\.\\d+)?") || blockType.equals("var")) {
                    lineNumContentBlock.setBackgroundResource(R.drawable.style_variable_hover);
                }
            }
            if (action == DragEvent.ACTION_DRAG_EXITED) {
                if (variables.contains(lineNumContentBlock.getText().toString())) {
                    lineNumContentBlock.setBackgroundResource(R.drawable.style_variable);
                }
                else {
                    lineNumContentBlock.setBackgroundResource(R.drawable.style_number);
                }
            }

            return true;
        }
    }

    /**
     * Long click listener for instruction block deletion functionality
     */
    public class DeleteListener implements View.OnLongClickListener {

        @Override
        public boolean onLongClick(View v){
            TextView toDeleteBlock = (TextView) v;

            final LinearLayout line = (LinearLayout) toDeleteBlock.getParent().getParent();
            final int lineNum = (int) line.getTag(); //get line number

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Delete Block")
                    .setMessage("Are you sure you want to delete the block at line " + Integer.toString(lineNum) + "?")
                    .setPositiveButton( "YES",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            line.removeViewAt(1);
                            lineContent[lineNum] = null;

                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("NO",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            builder.create().show();

            return true;
        }
    }

    /**
     * On click clear button method
     *
     * @param view default argument of view bound to and which executed the method
     */
    public void clearButton(View view){

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                .setTitle("Clear Program")
                .setMessage("Are you sure you want to clear the program grid?")
                .setPositiveButton( "YES",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        clearLines();

                        dialog.dismiss();
                    }
                })
                .setNegativeButton("NO",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder.create().show();
    }

    /**
     * On click stop button method
     *
     * @param view default argument of view bound to and which executed the method
     */
    public void stopButton(View view) {
        interrupt = true;
    }

    /**
     * On click run button method
     *
     * @param view default argument of view bound to and which executed the method
     */
    public void runButton(View view) {
        boolean valid = true;

        String[] cleanedProgram = new String[maxLineNo];
        int currentLine = 0;
        for (int i = 0; i < maxLineNo; ++i) {

            if (lineContent[i] != null) { //if line is not empty
                System.out.println(lineContent[i]); //for debugging

                if (lineContent[i].contains("#-")){ //if line has empty content blocks
                    valid = false; //program is not valid

                    addToConsole(". . . ");
                    addToConsole("ERROR: Empty content block(s)!\n   Ensure all content blocks are filled before running.\n   Exception encountered at line " + ((Integer)i).toString() + ".");
                    addToConsole("Program failed to parse!--");
                    addToConsole("");

                    break;
                }
                else { //else line is valid
                    cleanedProgram[currentLine++] = Integer.toString(i) + " " + lineContent[i]
                            .replace("#", "")
                            .replace("[", "")
                            .replace("]", "");
                }
            }
        }

        if (valid){ //if program is valid
            final String[] finalProgram = cleanedProgram; //inner class requires final variable reference

            addToConsole("--START--");

            new Thread(){
                public void run() {
                    interrupt = false;
                    error = false;

                    DumbbasicMain interpreter = new DumbbasicMain(MainActivity.this);
                    interpreter.execute(finalProgram);

                    if (!interrupt && !error) { //if execution was not interrupted and encountered no errors
                        addToConsole("--END--");
                    }
                    else {
                        if (!error) { //if interrupted with no errors
                            addToConsole(". . . ");
                        }
                        addToConsole("Execution was interrupted!--");
                    }

                    addToConsole(""); //add newline for visual purposes
                }
            }.start();

        }
    }
}