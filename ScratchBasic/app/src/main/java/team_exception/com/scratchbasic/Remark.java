package team_exception.com.scratchbasic;

/**
 * a "no-op" OP.  Can be useful as a GOTO target at the end of a program, or to hold comments.
 * 
 * @author Robert Merkel <robert.merkel@monash.edu>
 * 
 */
public class Remark extends Op {

	public Remark(int line ) {
		super(line);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(Context context) {
		context.setIp(nextln);
		// TODO Auto-generated method stub

	}

}
