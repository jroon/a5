package team_exception.com.scratchbasic;

/**
 * implements the GOTO operation
 *
 * @author Robert Merkel <robert.merkel@monash.edu>
 *
 */
public class Goto extends Op {

	private String target; // either a string representing a numeric constant, or a variable name
	/**
	 * creates a GOTO op
	 *
	 * @param line - the current line number
	 * @param targ - either a string literal of a number, or a variable name, which represents where we should GO TO
	 */
	public Goto(int line, String targ) {
		super(line); 
		target=targ;
		return;
	}

	@Override
	public void execute(Context context) throws Exception {
		int newline = context.getiValue(target);
		context.setIp(newline);
		return;
	}

}
